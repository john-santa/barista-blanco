package co.com.jasanta.petproject.baristablanco.domain.service;

import co.com.jasanta.petproject.baristablanco.domain.Product;
import co.com.jasanta.petproject.baristablanco.domain.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductServiceShould {

    private ProductService productService;
    private ProductRepository productRepository;

    @BeforeEach
    void setUp() {
        productRepository = Mockito.mock(ProductRepository.class);

        Mockito.when(
                productRepository.getAll())
                .thenReturn(
                        Arrays.asList(
                                new Product(
                                        1,
                                        "Guayaba Feijoa",
                                        1,
                                        300D,
                                        500,
                                        true
                                ),
                                new Product(
                                        2,
                                        "Mango",
                                        1,
                                        2100D,
                                        250,
                                        true
                                )
                        )
                );

        Mockito.when(productRepository
                .getProduct(Mockito.anyInt()))
                .thenReturn(
                        Optional.of(new Product(
                                1,
                                "Guayaba Feijoa",
                                1,
                                300D,
                                500,
                                true
                        ))
                );

        Mockito.when(productRepository
                .getByCategory(Mockito.anyInt()))
                .thenReturn(
                        Optional.of(
                                Arrays.asList(
                                        new Product(
                                                1,
                                                "Guayaba Feijoa",
                                                1,
                                                300D,
                                                500,
                                                true
                                        ),
                                        new Product(
                                                2,
                                                "Mango",
                                                1,
                                                2100D,
                                                250,
                                                true
                                        )
                                )
                        )
                );

        Mockito.when(productRepository
                .save(Mockito.any()))
                .thenReturn(
                        new Product(
                                1,
                                "Guayaba Feijoa",
                                1,
                                300D,
                                500,
                                true
                        )

                );

        productService = new ProductService(productRepository);
    }

    @Test
    void return_all_products() {
        List<Product> products = productService.getAll();
        Product expectedProduct = new Product(1, "Guayaba Feijoa", 1, 300D, 500, true);
        assertEquals(expectedProduct.getProductId(), products.get(0).getProductId());
        assertEquals(expectedProduct.getName(), products.get(0).getName());
        assertEquals(expectedProduct.getCategoryId(), products.get(0).getCategoryId());
        assertEquals(expectedProduct.getPrice(), products.get(0).getPrice());
        assertEquals(expectedProduct.getStock(), products.get(0).getStock());
        assertEquals(expectedProduct.isActive(), products.get(0).isActive());
    }

    @Test
    void return_product_by_id() {
        Optional<Product> product = productService.getProduct(Mockito.anyInt());
        Product expectedProduct = new Product(1, "Guayaba Feijoa", 1, 300D, 500, true);
        assertEquals(expectedProduct.getProductId(), product.stream().collect(Collectors.toList()).get(0).getProductId());
        assertEquals(expectedProduct.getName(), product.stream().collect(Collectors.toList()).get(0).getName());
        assertEquals(expectedProduct.getCategoryId(), product.stream().collect(Collectors.toList()).get(0).getCategoryId());
        assertEquals(expectedProduct.getPrice(), product.stream().collect(Collectors.toList()).get(0).getPrice());
        assertEquals(expectedProduct.getStock(), product.stream().collect(Collectors.toList()).get(0).getStock());
        assertEquals(expectedProduct.isActive(), product.stream().collect(Collectors.toList()).get(0).isActive());
    }

    @Test
    void return_products_by_category() {
        Optional<List<Product>> productsByCategory = productService.getByCategory(Mockito.anyInt());
        Product expectedProduct = new Product(1, "Guayaba Feijoa", 1, 300D, 500, true);
        assertEquals(expectedProduct.getProductId(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getProductId());
        assertEquals(expectedProduct.getName(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getName());
        assertEquals(expectedProduct.getCategoryId(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getCategoryId());
        assertEquals(expectedProduct.getPrice(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getPrice());
        assertEquals(expectedProduct.getStock(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getStock());
        assertEquals(expectedProduct.isActive(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).isActive());
    }

    @Test
    void return_product_save() {
        Product actualProduct = productService.save(new Product(1, "Guayaba Feijoa", 1, 300D, 500, true));
        Product expectedProduct = new Product(1, "Guayaba Feijoa", 1, 300D, 500, true);
        assertEquals(expectedProduct.getProductId(), actualProduct.getProductId());
        assertEquals(expectedProduct.getName(), actualProduct.getName());
        assertEquals(expectedProduct.getCategoryId(), actualProduct.getCategoryId());
        assertEquals(expectedProduct.getPrice(), actualProduct.getPrice());
        assertEquals(expectedProduct.getStock(), actualProduct.getStock());
        assertEquals(expectedProduct.isActive(), actualProduct.isActive());
    }
}
