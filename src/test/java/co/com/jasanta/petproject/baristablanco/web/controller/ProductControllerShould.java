package co.com.jasanta.petproject.baristablanco.web.controller;

import co.com.jasanta.petproject.baristablanco.domain.Product;
import co.com.jasanta.petproject.baristablanco.domain.service.ProductService;
import co.com.jasanta.petproject.web.controller.ProductController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductControllerShould {

    private ProductController productController;
    private ProductService productService;

    @BeforeEach
    void setUp() {
        productService = Mockito.mock(ProductService.class);

        Mockito.when(
                productService.getAll())
                .thenReturn(
                        Arrays.asList(
                                new Product(
                                        1,
                                        "Guayaba Feijoa",
                                        1,
                                        300D,
                                        500,
                                        true
                                ),
                                new Product(
                                        2,
                                        "Mango",
                                        1,
                                        2100D,
                                        250,
                                        true
                                )
                        )
                );

        Mockito.when(productService
                .getProduct(Mockito.anyInt()))
                .thenReturn(
                        Optional.of(new Product(
                                1,
                                "Guayaba Feijoa",
                                1,
                                300D,
                                500,
                                true
                        ))
                );

        Mockito.when(productService
                .getByCategory(Mockito.anyInt()))
                .thenReturn(
                        Optional.of(
                                Arrays.asList(
                                        new Product(
                                                1,
                                                "Guayaba Feijoa",
                                                1,
                                                300D,
                                                500,
                                                true
                                        ),
                                        new Product(
                                                2,
                                                "Mango",
                                                1,
                                                2100D,
                                                250,
                                                true
                                        )
                                )
                        )
                );

        Mockito.when(productService
                .save(Mockito.any()))
                .thenReturn(
                        new Product(
                                1,
                                "Guayaba Feijoa",
                                1,
                                300D,
                                500,
                                true
                        )

                );

        productController = new ProductController(productService);
    }

    @Test
    void return_all_products() {
        ResponseEntity<List<Product>> httpResponse = productController.getAll();
        List<Product> expectedList = Arrays.asList(
                new Product(
                        1,
                        "Guayaba Feijoa",
                        1,
                        300D,
                        500,
                        true
                ),
                new Product(
                        2,
                        "Mango",
                        1,
                        2100D,
                        250,
                        true
                )
        );
        assertEquals(HttpStatus.OK, httpResponse.getStatusCode());
        assertEquals(expectedList.get(0).getProductId(), Objects.requireNonNull(httpResponse.getBody()).get(0).getProductId());
    }

    @Test
    void return_product_by_id() {
        ResponseEntity<Product> httpResponse = productController.getProduct(Mockito.anyInt());
        Product expectedProduct = new Product(
                1,
                "Guayaba Feijoa",
                1,
                300D,
                500,
                true
        );
        assertEquals(HttpStatus.OK, httpResponse.getStatusCode());
        assertEquals(expectedProduct.getProductId(), Objects.requireNonNull(httpResponse.getBody()).getProductId());
    }

    @Test
    void return_products_by_category() {
        ResponseEntity<List<Product>> httpResponse = productController.getByCategory(Mockito.anyInt());
        List<Product> expectedList = Arrays.asList(
                new Product(
                        1,
                        "Guayaba Feijoa",
                        1,
                        300D,
                        500,
                        true
                ),
                new Product(
                        2,
                        "Mango",
                        1,
                        2100D,
                        250,
                        true
                )
        );
        assertEquals(HttpStatus.OK, httpResponse.getStatusCode());
        assertEquals(expectedList.get(0).getProductId(), Objects.requireNonNull(httpResponse.getBody()).get(0).getProductId());

    }

    @Test
    void return_product_save() {
        ResponseEntity<Product> httpResponse = productController.save(new Product(1, "Guayaba Feijoa", 1, 300D, 500, true));
        Product expectedProduct = new Product(
                1,
                "Guayaba Feijoa",
                1,
                300D,
                500,
                true
        );
        assertEquals(HttpStatus.CREATED, httpResponse.getStatusCode());
        assertEquals(expectedProduct.getProductId(), Objects.requireNonNull(httpResponse.getBody()).getProductId());
    }
}
