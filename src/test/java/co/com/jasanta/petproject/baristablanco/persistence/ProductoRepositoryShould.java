package co.com.jasanta.petproject.baristablanco.persistence;

import co.com.jasanta.petproject.baristablanco.domain.Product;
import co.com.jasanta.petproject.baristablanco.persistence.crud.ProductoCrudRepository;
import co.com.jasanta.petproject.baristablanco.persistence.entity.Producto;
import co.com.jasanta.petproject.baristablanco.persistence.mapper.ProductMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class ProductoRepositoryShould {

    private ProductoRepository productoRepository;
    private ProductoCrudRepository crudRepository;
    private ProductMapper mapper;

    @BeforeEach
    void setUp() {
        crudRepository = Mockito.mock(ProductoCrudRepository.class);
        mapper = Mockito.mock(ProductMapper.class);

        Mockito.when(crudRepository
                .findAll())
                .thenReturn(
                        Arrays.asList(
                                new Producto(
                                        1,
                                        "Guayaba Feijoa",
                                        1,
                                        "7029 A42 23",
                                        300D,
                                        500,
                                        true
                                ),
                                new Producto(
                                        2,
                                        "Mango",
                                        1,
                                        "0316 R56 01",
                                        2100D,
                                        250,
                                        true
                                )
                        )
                );
        Mockito.when(crudRepository
                .findByIdCategoriaOrderByNombreAsc(Mockito.anyInt()))
                .thenReturn(
                        Arrays.asList(
                                new Producto(
                                        1,
                                        "Guayaba Feijoa",
                                        1,
                                        "7029 A42 23",
                                        300D,
                                        500,
                                        true
                                ),
                                new Producto(
                                        2,
                                        "Mango",
                                        1,
                                        "0316 R56 01",
                                        2100D,
                                        250,
                                        true
                                )
                        )
                );

        Mockito.when(crudRepository
                .findByCantidadStockLessThanAndEstado(Mockito.anyInt(), Mockito.anyBoolean()))
                .thenReturn(
                        Optional.of(
                                Arrays.asList(
                                        new Producto(
                                                1,
                                                "Guayaba Feijoa",
                                                1,
                                                "7029 A42 23",
                                                300D,
                                                500,
                                                true
                                        ),
                                        new Producto(
                                                2,
                                                "Mango",
                                                1,
                                                "0316 R56 01",
                                                2100D,
                                                250,
                                                true
                                        )
                                )
                        )
                );

        Mockito.when(crudRepository
                .findById(Mockito.anyInt()))
                .thenReturn(
                        Optional.of(
                                new Producto(
                                        1,
                                        "Guayaba Feijoa",
                                        1,
                                        "7029 A42 23",
                                        300D,
                                        500,
                                        true
                                )
                        )
                );

        Mockito.when(crudRepository
                .save(Mockito.any()))
                .thenReturn(
                        new Producto(
                                1,
                                "Guayaba Feijoa",
                                1,
                                "7029 A42 23",
                                300D,
                                500,
                                true
                        )

                );

        Mockito.when(mapper
                .toProducts(Mockito.anyList()))
                .thenReturn(
                        Arrays.asList(
                                new Product(
                                        1,
                                        "Guayaba Feijoa",
                                        1,
                                        300D,
                                        500,
                                        true
                                ),
                                new Product(
                                        2,
                                        "Mango",
                                        1,
                                        2100D,
                                        250,
                                        true
                                )
                        )
                );

        Mockito.when(mapper
                .toProduct(Mockito.any()))
                .thenReturn(
                        new Product(
                                1,
                                "Guayaba Feijoa",
                                1,
                                300D,
                                500,
                                true
                        )
                );

        Mockito.when(mapper
                .toProducto(Mockito.any()))
                .thenReturn(
                        new Producto(
                                1,
                                "Guayaba Feijoa",
                                1,
                                "7029 A42 23",
                                300D,
                                500,
                                true
                        )
                );

        productoRepository = new ProductoRepository(crudRepository, mapper);
    }

    @Test
    void return_all_products() {

        List<Product> products = productoRepository.getAll();
        Product expectedProduct = new Product(1, "Guayaba Feijoa", 1, 300D, 500, true);
        assertEquals(expectedProduct.getProductId(), products.get(0).getProductId());
        assertEquals(expectedProduct.getName(), products.get(0).getName());
        assertEquals(expectedProduct.getCategoryId(), products.get(0).getCategoryId());
        assertEquals(expectedProduct.getPrice(), products.get(0).getPrice());
        assertEquals(expectedProduct.getStock(), products.get(0).getStock());
        assertEquals(expectedProduct.isActive(), products.get(0).isActive());
    }

    @Test
    void return_products_by_category() {

        Optional<List<Product>> productsByCategory = productoRepository.getByCategory(Mockito.anyInt());
        Product expectedProduct = new Product(1, "Guayaba Feijoa", 1, 300D, 500, true);

        assertEquals(expectedProduct.getProductId(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getProductId());
        assertEquals(expectedProduct.getName(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getName());
        assertEquals(expectedProduct.getCategoryId(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getCategoryId());
        assertEquals(expectedProduct.getPrice(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getPrice());
        assertEquals(expectedProduct.getStock(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getStock());
        assertEquals(expectedProduct.isActive(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).isActive());
    }

    @Test
    void return_products_by_stock_lessthan_and_status_active() {
        Optional<List<Product>> productsByCategory = productoRepository.getScarseProducts(100, true);
        Product expectedProduct = new Product(1, "Guayaba Feijoa", 1, 300D, 500, true);
        assertEquals(expectedProduct.getProductId(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getProductId());
        assertEquals(expectedProduct.getName(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getName());
        assertEquals(expectedProduct.getCategoryId(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getCategoryId());
        assertEquals(expectedProduct.getPrice(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getPrice());
        assertEquals(expectedProduct.getStock(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).getStock());
        assertEquals(expectedProduct.isActive(), productsByCategory.stream().collect(Collectors.toList()).get(0).get(0).isActive());
    }

    @Test
    void return_product_by_id() {
        Optional<Product> product = productoRepository.getProduct(Mockito.anyInt());
        Product expectedProduct = new Product(1, "Guayaba Feijoa", 1, 300D, 500, true);
        assertEquals(expectedProduct.getProductId(), product.stream().collect(Collectors.toList()).get(0).getProductId());
        assertEquals(expectedProduct.getName(), product.stream().collect(Collectors.toList()).get(0).getName());
        assertEquals(expectedProduct.getCategoryId(), product.stream().collect(Collectors.toList()).get(0).getCategoryId());
        assertEquals(expectedProduct.getPrice(), product.stream().collect(Collectors.toList()).get(0).getPrice());
        assertEquals(expectedProduct.getStock(), product.stream().collect(Collectors.toList()).get(0).getStock());
        assertEquals(expectedProduct.isActive(), product.stream().collect(Collectors.toList()).get(0).isActive());
    }

    @Test
    void return_product_save() {
        Product actualProduct = productoRepository.save(new Product(1, "Guayaba Feijoa", 1, 300D, 500, true));
        Product expectedProduct = new Product(1, "Guayaba Feijoa", 1, 300D, 500, true);
        assertEquals(expectedProduct.getProductId(), actualProduct.getProductId());
        assertEquals(expectedProduct.getName(), actualProduct.getName());
        assertEquals(expectedProduct.getCategoryId(), actualProduct.getCategoryId());
        assertEquals(expectedProduct.getPrice(), actualProduct.getPrice());
        assertEquals(expectedProduct.getStock(), actualProduct.getStock());
        assertEquals(expectedProduct.isActive(), actualProduct.isActive());
    }
}
