package co.com.jasanta.petproject.baristablanco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaristaBlancoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaristaBlancoApplication.class, args);
    }

}
