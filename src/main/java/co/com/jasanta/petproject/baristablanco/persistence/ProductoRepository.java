package co.com.jasanta.petproject.baristablanco.persistence;

import co.com.jasanta.petproject.baristablanco.domain.Product;
import co.com.jasanta.petproject.baristablanco.domain.repository.ProductRepository;
import co.com.jasanta.petproject.baristablanco.persistence.crud.ProductoCrudRepository;
import co.com.jasanta.petproject.baristablanco.persistence.entity.Producto;
import co.com.jasanta.petproject.baristablanco.persistence.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductoRepository implements ProductRepository {
    @Autowired
    private final ProductoCrudRepository productoCrudRepository;

    @Autowired
    private final ProductMapper mapper;

    public ProductoRepository(ProductoCrudRepository productoCrudRepository, ProductMapper mapper) {
        this.productoCrudRepository = productoCrudRepository;
        this.mapper = mapper;
    }

    @Override
    public List<Product> getAll() {
        List<Producto> productos = (List<Producto>) productoCrudRepository.findAll();
        return mapper.toProducts(productos);
    }

    @Override
    public Optional<List<Product>> getByCategory(int categoryId) {
        List<Producto> productos = productoCrudRepository.findByIdCategoriaOrderByNombreAsc(categoryId);
        return Optional.of(mapper.toProducts(productos));
    }

    @Override
    public Optional<List<Product>> getScarseProducts(int quantity, boolean active) {
        Optional<List<Producto>> productos = productoCrudRepository.findByCantidadStockLessThanAndEstado(quantity, active);
        return productos.map(mapper::toProducts);
    }

    @Override
    public Optional<Product> getProduct(int productId) {
        return productoCrudRepository.findById(productId).map(mapper::toProduct);
    }

    @Override
    public Product save(Product product) {
        var producto = mapper.toProducto(product);
        return mapper.toProduct(productoCrudRepository.save(producto));
    }
}
